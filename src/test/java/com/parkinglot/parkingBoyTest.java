package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class parkingBoyTest {
    @Test
    void should_return_a_parking_ticket_when_park_given_a_parking_lot_and_a_car_and_parking_boy() {
        // Given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        // When
        Ticket ticket = parkingBoy.park(car);
        // Then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_park_given_a_parking_ticket_with_a_parked_and_a_car_and_parking_boy() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = parkingBoy.park(car1);
        // When
        Car car2 = parkingBoy.fetch(ticket);
        // Then
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_return_Unrecognized_parking_tick_when_park_given_a_used_ticket_and_parking_boy() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = parkingBoy.park(car1);
        // When
        Car car2 = parkingBoy.fetch(ticket);
        Assertions.assertNotNull(car2);
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingBoy.fetch(ticket));
        // Then
        Assertions.assertEquals("Unrecognized parking tick", exception.getMessage());
    }

    @Test
    void should_return_Unrecognized_parking_tick_when_fetch_given_a_wrong_ticket_and_parking_boy() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = new Ticket(car);
        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingBoy.fetch(ticket));
        // Then
        Assertions.assertEquals("Unrecognized parking tick", exception.getMessage());
    }

    @Test
    void should_return_No_available_positions_when_park_given_full_parking_lot_and_parking_boy() {
        // Given
        ParkingLot parkingLot = new ParkingLot(0);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();
        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingBoy.park(car));
        // Then
        Assertions.assertEquals("No available positions", exception.getMessage());
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_given_a_parking_boy_and_two_parked_cars_and_two_parking_ticker() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot);
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingBoy.park(car1);
        Ticket ticket2 = parkingBoy.park(car2);
        // When
        Car car3 = parkingBoy.fetch(ticket1);
        Car car4 = parkingBoy.fetch(ticket2);
        // Then
        Assertions.assertEquals(car1, car3);
        Assertions.assertEquals(car2, car4);
    }

    // Story 4
    @Test
    void should_return_a_sequential_parking_ticket_when_park_given_a_parking_boy_and_two_not_full_parking_lots_and_a_car() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        // When
        Ticket ticket1 = parkingBoy.park(car1);
        // Then
        Car car2 = parkingLot1.fetch(ticket1);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_return_a_sequential_parking_ticket_when_park_given_a_parking_boy_and_a_full_parking_lot_and_a_not_full_parking_lot_and_a_car() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(10);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        // When
        Ticket ticket = parkingBoy.park(car1);
        // Then
        Car car2 = parkingLot2.fetch(ticket);
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_return_No_available_positions_ticket_when_park_given_a_parking_boy_and_two_full_parking_lots_and_a_car() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();
        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingBoy.park(car));
        // Then
        Assertions.assertEquals("No available positions", exception.getMessage());
    }

    @Test
    void should_return_Unrecognized_parking_tick_when_fetch_given_a_parking_boy_and_two_parking_lots_and_a_wrong_ticket() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = new Ticket(car);
        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingBoy.fetch(ticket));
        // Then
        Assertions.assertEquals("Unrecognized parking tick", exception.getMessage());
    }

    @Test
    void should_return_Unrecognized_parking_tick_when_fetch_given_a_parking_boy_and_two_parking_lots_and_a_used_ticket() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = parkingBoy.park(car1);
        Car car2 = parkingBoy.fetch(ticket);

        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingBoy.fetch(ticket));
        // Then
        Assertions.assertEquals("Unrecognized parking tick", exception.getMessage());
    }
    @Test
    void should_return_a_car_when_fetch_given_a_parking_boy_and_two_parking_lots_and_a_parking_ticket() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        ParkingBoy parkingBoy = new ParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = parkingBoy.park(car1);

        // When
        Car car2 = parkingBoy.fetch(ticket);
        // Then
        Assertions.assertEquals(car1, car2);
    }
}
