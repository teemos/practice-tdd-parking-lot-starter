package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class parkingLotTest {
    @Test
    void should_return_a_parking_ticket_when_park_given_a_parking_lot_and_a_car() {
        // Given
        Car car = new Car();
        ParkingLot parkingLot = new ParkingLot(10);
        // When
        Ticket ticket = parkingLot.park(car);
        // Then
        Assertions.assertNotNull(ticket);
    }

    @Test
    void should_return_a_car_when_park_given_a_parking_ticket_with_a_parked() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Ticket ticket = parkingLot.park(car1);
        // When
        Car car2 = parkingLot.fetch(ticket);
        // Then
        Assertions.assertEquals(car1, car2);
    }

    @Test
    void should_return_Unrecognized_parking_tick_when_park_given_a_used_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Ticket ticket = parkingLot.park(car1);
        // When
        Car car2 = parkingLot.fetch(ticket);
        Assertions.assertNotNull(car2);
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingLot.fetch(ticket));
        // Then
        Assertions.assertEquals("Unrecognized parking tick", exception.getMessage());
    }

    // 4
    @Test
    void should_return_Unrecognized_parking_tick_when_fetch_given_a_wrong_ticket() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car = new Car();
        Ticket ticket = new Ticket(car);
        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingLot.fetch(ticket));
        // Then
        Assertions.assertEquals("Unrecognized parking tick", exception.getMessage());
    }

    @Test
    void should_return_No_available_positions_when_park_given_full_parking_lot() {
        // Given
        ParkingLot parkingLot = new ParkingLot(0);
        Car car = new Car();
        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> parkingLot.park(car));
        // Then
        Assertions.assertEquals("No available positions", exception.getMessage());
    }

    @Test
    void should_return_the_right_car_with_each_ticket_when_fetch_given_two_parked_cars_and_two_parking_ticker() {
        // Given
        ParkingLot parkingLot = new ParkingLot(10);
        Car car1 = new Car();
        Car car2 = new Car();
        Ticket ticket1 = parkingLot.park(car1);
        Ticket ticket2 = parkingLot.park(car2);
        // When
        Car car3 = parkingLot.fetch(ticket1);
        Car car4 = parkingLot.fetch(ticket2);
        // Then
        Assertions.assertEquals(car1, car3);
        Assertions.assertEquals(car2, car4);
    }
}
