package com.parkinglot;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

public class smartParkingBoyTest {
    @Test
    void should_return_a_sequential_parking_ticket_when_park_given_a_smart_parking_boy_and_one_is_less_and_two_is_more_parking_lots_and_a_car() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(100);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartParkingBoy = new SmartParkingBoy(parkingLots);
        Car car1 = new Car();
        // When
        Ticket ticket1 = smartParkingBoy.park(car1);
        // Then
        Car car2 = parkingLot2.fetch(ticket1);
        Assertions.assertEquals(car1, car2);
    }
    @Test
    void should_return_No_available_positions_ticket_when_park_given_a_smart_parking_boy_and_two_full_parking_lots_and_a_car() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(0);
        ParkingLot parkingLot2 = new ParkingLot(0);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartparkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();
        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> smartparkingBoy.park(car));
        // Then
        Assertions.assertEquals("No available positions", exception.getMessage());
    }
    @Test
    void should_return_Unrecognized_parking_tick_when_fetch_given_a_smart_parking_boy_and_two_parking_lots_and_a_wrong_ticket() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartparkingBoy = new SmartParkingBoy(parkingLots);
        Car car = new Car();
        Ticket ticket = new Ticket(car);
        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> smartparkingBoy.fetch(ticket));
        // Then
        Assertions.assertEquals("Unrecognized parking tick", exception.getMessage());
    }
    @Test
    void should_return_Unrecognized_parking_tick_when_fetch_given_a_smart_parking_boy_and_two_parking_lots_and_a_used_ticket() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartparkingBoy = new SmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = smartparkingBoy.park(car1);
        Car car2 = smartparkingBoy.fetch(ticket);

        // When
        RuntimeException exception = Assertions.assertThrows(RuntimeException.class,
                () -> smartparkingBoy.fetch(ticket));
        // Then
        Assertions.assertEquals("Unrecognized parking tick", exception.getMessage());
    }
    @Test
    void should_return_a_car_when_fetch_given_a_smart_parking_boy_and_two_parking_lots_and_a_parking_ticket() {
        // Given
        ParkingLot parkingLot1 = new ParkingLot(10);
        ParkingLot parkingLot2 = new ParkingLot(10);

        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(parkingLot1);
        parkingLots.add(parkingLot2);

        SmartParkingBoy smartparkingBoy = new SmartParkingBoy(parkingLots);
        Car car1 = new Car();
        Ticket ticket = smartparkingBoy.park(car1);

        // When
        Car car2 = smartparkingBoy.fetch(ticket);
        // Then
        Assertions.assertEquals(car1, car2);
    }
}
