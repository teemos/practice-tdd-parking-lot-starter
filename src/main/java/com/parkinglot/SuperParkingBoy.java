package com.parkinglot;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SuperParkingBoy {
    private List<ParkingLot> parkingLots;

    public SuperParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    private double calculateAvailablePositionRate(ParkingLot parkingLot) {
        return (double) parkingLot.remainingCapacity() / parkingLot.getCapacity();
    }

    public Ticket park(Car car) {
//        double largerAvailablePositionRate = 0;
//
//        ParkingLot rightParkingLot = null;
//
//        for (ParkingLot parkingLot : parkingLots) {
//            if (!parkingLot.isFull()) {
//                double availablePositionRate = (double) parkingLot.remainingCapacity() / parkingLot.getCapacity();
//                if (availablePositionRate >= largerAvailablePositionRate) {
//                    largerAvailablePositionRate = availablePositionRate;
//                    rightParkingLot = parkingLot;
//                }
//            }
//        }
//        if (rightParkingLot != null) {
//            return rightParkingLot.park(car);
//        }
        Optional<ParkingLot> rightParkingLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .max(Comparator.comparingDouble(this::calculateAvailablePositionRate));

        if (rightParkingLot.isPresent()) {
            return rightParkingLot.get().park(car);
        }
        throw new RuntimeException("No available positions");
    }

    public Car fetch(Ticket ticket) {
//        for (ParkingLot parkingLot : parkingLots) {
//            if (!parkingLot.isInValidTicket(ticket)) {
//                return parkingLot.fetch(ticket);
//            }
//        }
        Optional<Car> fetchedCar = parkingLots.stream()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .filter(Objects::nonNull)
                .findFirst();

        if (fetchedCar.isPresent()) {
            return fetchedCar.get();
        }
        throw new RuntimeException("Unrecognized parking tick");
    }
}
