package com.parkinglot;

import java.util.List;
import java.util.Optional;

public class ParkingBoy {
    private List<ParkingLot> parkingLots;


    public ParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
//        for (ParkingLot parkingLot : parkingLots) {
//            if (!parkingLot.isFull()) {
//                return parkingLot.park(car);
//            }
//        }
//        throw new RuntimeException("No available positions");
        Optional<ParkingLot> availableLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .findFirst();

        if (availableLot.isPresent()) {
            return availableLot.get().park(car);
        }

        throw new RuntimeException("No available positions");
    }

    public Car fetch(Ticket ticket) {
//        for (ParkingLot parkingLot : parkingLots) {
//            if (!parkingLot.isInValidTicket(ticket)) {
//                return parkingLot.fetch(ticket);
//            }
//        }
        Optional<ParkingLot> validLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isInValidTicket(ticket))
                .findFirst();

        if (validLot.isPresent()) {
            return validLot.get().fetch(ticket);
        }
        throw new RuntimeException("Unrecognized parking tick");
    }
}
