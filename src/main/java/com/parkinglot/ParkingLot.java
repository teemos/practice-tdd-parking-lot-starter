package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {
    private HashMap<Ticket, Car> ticketMap;
    private int capacity = 10;

    public ParkingLot(int capacity) {
        this.ticketMap = new HashMap<>();
        this.capacity = capacity;
    }

    public boolean isFull() {
        return ticketMap.size() >= this.capacity;
    }

    public boolean isInValidTicket(Ticket ticket) {
        return ticketMap.get(ticket) == null;
    }

    public int getCapacity() {
        return this.capacity;
    }

    public int remainingCapacity() {
        return this.capacity - ticketMap.size();
    }

    public Ticket park(Car car) {
        if (isFull()) {
            throw new RuntimeException("No available positions");
        }
        Ticket ticket = new Ticket(car);
        ticketMap.put(ticket, car);
        return ticket;
    }

    public Car fetch(Ticket ticket) {
        if (isInValidTicket(ticket)) {
            throw new RuntimeException("Unrecognized parking tick");
        }
        Car car = ticketMap.get(ticket);
        ticketMap.remove(ticket);
        return car;
    }
}
