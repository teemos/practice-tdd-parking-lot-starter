package com.parkinglot;

import java.util.Comparator;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

public class SmartParkingBoy {
    private List<ParkingLot> parkingLots;

    public SmartParkingBoy(List<ParkingLot> parkingLots) {
        this.parkingLots = parkingLots;
    }

    public Ticket park(Car car) {
//        int remainingCapacity = 0;
//        ParkingLot rightParkingLot = null;
//
//        for (ParkingLot parkingLot : parkingLots) {
//            if (!parkingLot.isFull()) {
//                if (parkingLot.remainingCapacity() >= remainingCapacity) {
//                    remainingCapacity = parkingLot.remainingCapacity();
//                    rightParkingLot = parkingLot;
//                }
//            }
//        }
//        if (rightParkingLot != null) {
//            return rightParkingLot.park(car);
//        }
        Optional<ParkingLot> rightParkingLot = parkingLots.stream()
                .filter(parkingLot -> !parkingLot.isFull())
                .max(Comparator.comparingInt(ParkingLot::remainingCapacity));

        if (rightParkingLot.isPresent()) {
            return rightParkingLot.get().park(car);
        }

        throw new RuntimeException("No available positions");
    }

    public Car fetch(Ticket ticket) {
//        for (ParkingLot parkingLot : parkingLots) {
//            if (!parkingLot.isInValidTicket(ticket)) {
//                return parkingLot.fetch(ticket);
//            }
//        }
        Optional<Car> fetchedCar = parkingLots.stream()
                .map(parkingLot -> parkingLot.fetch(ticket))
                .filter(Objects::nonNull)
                .findFirst();

        if (fetchedCar.isPresent()) {
            return fetchedCar.get();
        }

        throw new RuntimeException("Unrecognized parking tick");
    }
}
