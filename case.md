# Story 1
1. Given a parking lot, and a car, When park the car, Then return a parking ticket.
2. Given a parking lot with a parked car, When fetch the car, Then return a car.
3. Given a parking lot and a used ticket, When fetch the car, Then return "null".
4. Given a parking lot and a wrong ticket, When fetch the car, Then return "null".
5. Given a car and full parking lots, When park the car, Then return "null".
6. Given a parking lot with two parked cars and two parking tickets, When fetch the car twice, Then return the right car with each ticket.
# Story 2
1. Given a parking lot and a used ticket, When fetch the car, Then return "Unrecognized parking tick".
2. Given a parking lot and a wrong ticket, When fetch the car, Then return "Unrecognized parking tick".
3. Given a car and full parking lots, When park the car, Then return "No available positions".
#   Story 3
1. Given a parking lot, a parking boy and a car, When park the car, Then return a parking ticket.
2. Given a parking lot with a parked car, a parking boy, When fetch the car, Then return a car.
3. Given a parking lot , a parking boy, and a used ticket, When fetch the car, Then return "Unrecognized parking tick".
4. Given a parking lot, a parking boy,  and a wrong ticket, When fetch the car, Then return "Unrecognized parking tick".
5. Given a car, a parking boy,  and full parking lots, When park the car, Then return "No available positions".
6. Given a parking lot with two parked cars , a parking boy and two parking tickets, When fetch the car twice, Then return the right car with each ticket.
# Story 4
1. Given a parking boy,  two parkinglots (all are not full), a car, When park the car, Then return a sequential parking ticket.
2. Given a parking boy,  two parkinglots (one is full and two is not full)), a car , When park the car, Then return a sequential parking ticket.
3. Given a parking boy,  two full parkinglots, a car , When park the car, Then return "No available positions".
4. Given a parking boy,  two parkinglots,and a wrong ticket When fetch the car, Then return "Unrecognized parking tick".
5. Given a parking boy,  two parkinglots,and a used ticket When fetch the car, Then return "Unrecognized parking tick".
6. Given a parking boy,  two parkinglots,and a parking ticket When fetch the car, Then return a car.
# Story 5
1. Given a smart parking boy,  two parkinglots (one is less, one is more), a car, When park the car, Then return a sequential parking ticket.
2. Given a smart parking boy,  two full parkinglots, a car , When park the car, Then return "No available positions".
3. Given a smart parking boy,  two parkinglots,and a wrong ticket When fetch the car, Then return "Unrecognized parking tick".
4. Given a smart parking boy,  two parkinglots,and a used ticket When fetch the car, Then return "Unrecognized parking tick".
5. Given a smart parking boy,  two parkinglots,and a parking ticket When fetch the car, Then return a car.
# Story 6
1. Given a super parking boy,  two parkinglots (one is less, one is more), a car, When park the car, Then return a sequential parking ticket.
2. Given a super parking boy,  two full parkinglots, a car , When park the car, Then return "No available positions".
3. Given a super parking boy,  two parkinglots,and a wrong ticket When fetch the car, Then return "Unrecognized parking tick".
4. Given a super parking boy,  two parkinglots,and a used ticket When fetch the car, Then return "Unrecognized parking tick".
5. Given a super parking boy,  two parkinglots,and a parking ticket When fetch the car, Then return a car.