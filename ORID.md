# ORID
## O
- TDD-OO: Today we will continue to learn TDD. Unlike yesterday, today's TDD has added object-oriented content. Today, the teacher talked less and mainly focused on our own practice. From morning to afternoon, we were pounding on the code.
# R
OK
## I
I think this effect is very good. After learning a knowledge point, conducting a lot of practice can deepen the impression of knowledge.
## D
When writing code in the future, I will try using the TDD to write code instead of just like before.